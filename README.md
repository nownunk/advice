## Get started

**NOTE**: Our running environment is 
```
Ubuntu: 16.04
PyTorch: 1.7 
CUDA: 10.1
GPU: TITAN Xp
```

### STEP 1. Prerequisite

Install bert-sklearn --
a scikit-learn wrapper to finetune BERT model based on the Huggingface's pytorch transformer.
```
git clone -b master https://github.com/charles9n/bert-sklearn
cd bert-sklearn
pip install .
```

Install other packages
```
pip install fire
```


### STEP 2. Get the repo, including code and data
```
git clone https://bitbucket.org/nownunk/advice
cd advice
```

### STEP 3. (Check STEP 4 for an alternative approach)
```
cd code
```
**NOTE:** all the following commands are assembled in `code/Makefile`

#### 3.1 To evaluate the performance of the model, say, 5-fold cross-validation 

First, generate a prediction file as the result of training and testing each of the 5 folds
```
python run.py advice_classifier --task=train_KFold_model
```
This will take as input the annotated dataset `data/annotations.csv`,
and assemble the prediction results from each fold into `code/working/pred/[TAG]_train_K5_epochs3.csv`
Also, 5 fine-tuned BERT models will be saved at `code/working/model/[TAG]_K5_epochs3_[fold].bin`

Second, display the evaluation results

```
python run.py advice_classifier --task=evaluate_and_error_analysis
```

#### 3.2 Apply the above fine-tuned models to the unseen sentences

For the unstructured abstracts:
```
python run.py advice_classifier --task=apply_trained_model_to_discussion_sentences
```
For the discussion/conclusion sections:

// **Check STEP 4 for an alternative approach**
```
python run.py advice_classifier --task=apply_trained_model_to_unstructured_abstract_sentences
```

#### 3.3 Postprocess the above results with filtering rules

For the unstructured abstracts:
```
python run.py advice_classifier --task=postprocessing_filter_for_discussions
```
For the discussion/conclusion sections:
```
python run.py advice_classifier --task=postprocessing_filter_for_unstructured_abstracts
```

### 4 An alternative approach for predicting discussion sentences

This is an approach of augmenting training data with part of annotated discussion sentences,
and then applying the trained model to the remaining discussion sentences.
First, we split the discussion sentences into K groups (grouped by paper ID).
For each run (of K runs), the training data consists of all structured abstract sentences 
and the discussion sentences of 1/K papers. The newly constructed text will take the following form:
```
section name [SEP] citation mentioned [SEP] past tense [SEP] study info [SEP] The original sentence
```

Specifically, for structured abstract sentences:
```
structured abstract [SEP]  [SEP]  [SEP]  [SEP] The original sentence
```

And for discussion sentences:
```
discussion [SEP] No [SEP] Yes [SEP] No [SEP] The original sentence
```

Run the following command
```
python mixed_train.py
```

Result (after assembling results from the K=5 runs):
```
              precision    recall  f1-score   support

           0      0.991     0.991     0.991      3635
           1      0.838     0.895     0.866       162
           2      0.912     0.844     0.877       135

    accuracy                          0.982      3932
   macro avg      0.914     0.910     0.911      3932
```
