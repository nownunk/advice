import os, sys, time
import numpy as np
import pandas as pd
from IPython.display import display
from sklearn.metrics import accuracy_score, classification_report, precision_recall_fscore_support, confusion_matrix
from sklearn.model_selection import StratifiedKFold, GroupKFold
from bert_sklearn import BertClassifier

NVIDIA_TITAN_XP = "0"
NVIDIA_1080TI = "1"
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = NVIDIA_1080TI
os.environ["CUDA_VISIBLE_DEVICES"] = NVIDIA_TITAN_XP

print('GPU =', os.environ["CUDA_VISIBLE_DEVICES"])

pd.options.display.max_colwidth = 120
pd.options.display.width = 200
pd.options.display.precision = 3
np.set_printoptions(precision=3)

BERT_NAME_2_MODEL = {'bert' : 'bert-base-cased', 'biobert' : 'biobert-base-cased' }
BERT_MODEL_NAME = 'biobert'
BERT_MODEL = BERT_NAME_2_MODEL[BERT_MODEL_NAME]

PRJ = 'advice'
label_name = {0:'None', 1:'Weak', 2:'Strong'}
NUM_CLASSES = len(label_name)

K = 5
EPOCHS = 3

#TEXT_column = "sentence"
TEXT_column = "text_SEP"

#SPLIT_by = "stratified"
SPLIT_by = "pmid"

print(f'\nK={K}  EPOCHS={EPOCHS}  text_column={TEXT_column}  split_by={SPLIT_by}\n')

DATA_DIR = '../data'
MODEL_DIR = f'working2/model/{PRJ}_{BERT_MODEL_NAME}'

RANDOM_STATE = 0

if 1:
    FIELDS_to_use = []

    use_section = True
    use_citation = True
    use_past_tense = True
    use_study_info = True

    if use_section:
        FIELDS_to_use.append('section')
    if use_citation:
        FIELDS_to_use.append('citation_mentioned')
    if use_past_tense:
        FIELDS_to_use.append('past_tense')
    if use_study_info:
        FIELDS_to_use.append('study_info')

    use_rel_loc = False
    if use_rel_loc:
        FIELDS_to_use.append('rel_loc')

    FIELDS_to_use.append('sentence')
    print('\n- Fields used:', FIELDS_to_use, '\n')


##################################
#
# functions
#

def get_model_bin_file(fold=0):
    if not os.path.exists(MODEL_DIR):
        os.makedirs(MODEL_DIR)
        print(f'\ncreate a new folder for storing BERT model: "{MODEL_DIR}"\n')
    if fold>=0:
        return f'{MODEL_DIR}/K{K}_epochs{EPOCHS}_{fold}.bin'
    else:
        print('Wrong value of fold:', fold)
        sys.exit()

def get_pred_csv_file(mode='train'):
    pred_folder = f'working2/pred/{PRJ}_{BERT_MODEL_NAME}_{mode}'
    if not os.path.exists(pred_folder):
        os.makedirs(pred_folder)
        print('\ncreate new folder for prediction results:', pred_folder, '\n')
    if mode == 'train':
        return f'{pred_folder}/K{K}_epochs{EPOCHS}.csv'
    elif mode == 'apply' or mode == 'ensemble':
        return f'{pred_folder}/epochs{EPOCHS}.csv'
    else:
        print('- wrong mode:', mode, '\n')


def gen_text_SEP(x):
    #x['rel_loc'] = f"{int(x['rel_loc']*5)}" if type(x['rel_loc'])==float else x['rel_loc']
    x['rel_loc'] = f"{x['rel_loc']:.2f}" if type(x['rel_loc'])==float else x['rel_loc']

    convert_map = {'0':'No', '1':'Yes'}
    def convert_01_to_words(s):
        s = str(s)
        return convert_map[s] if s in convert_map else s
    return " [SEP] ".join([convert_01_to_words(x[col]) for col in FIELDS_to_use])

def get_train_test_data(fold):
    missing_value = ''
    missing_value_for_rel_loc = ''
    section_structured_abstract = 'structured abstract'
    section_discussion = 'discussion'
    df_train_6k = pd.read_csv(f'{DATA_DIR}/annotations.csv')
    df_train_6k['section'] = section_structured_abstract
    df_train_6k['citation_mentioned'] = missing_value
    df_train_6k['past_tense'] = missing_value
    df_train_6k['study_info'] = missing_value
    df_train_6k['rel_loc'] = missing_value_for_rel_loc

    df_discussion = pd.read_csv(f'{DATA_DIR}/annotations_discussion.csv')
    df_discussion = df_discussion.sample(frac=1, random_state=0)
    df_discussion['section'] = section_discussion

    if SPLIT_by == 'pmid':
        skf = GroupKFold(n_splits=K) # no parameter for random_state which generates fixed result
        splits = skf.split(df_discussion.sentence, df_discussion.label, df_discussion.pmid)
    elif SPLIT_by == 'stratified':
        skf = StratifiedKFold(n_splits=K, shuffle=True, random_state=0)
        splits = skf.split(df_discussion.sentence, df_discussion.label)
    else:
        print(f'- wrong SPLIT_by: {SPLIT_by}')
        sys.exit()

    for i, (train_index, test_index) in enumerate(splits):
        if i != fold: continue
        df_discussion_train = df_discussion.iloc[train_index]
        df_discussion_test = df_discussion.iloc[test_index]
        df_train = pd.concat((df_train_6k[df_discussion.columns], df_discussion_train))

    train = df_train.copy()
    test = df_discussion_test.copy()
    print(f'- train size: {len(train)}      test size: {len(test)}\n')

    train.fillna('', inplace=True)
    test.fillna('', inplace=True)
    if TEXT_column == 'text_SEP':
        train[TEXT_column] = train.apply(gen_text_SEP, axis=1)
        test[TEXT_column] = test.apply(gen_text_SEP, axis=1)
    print(train.iloc[0][TEXT_column][:80], '...')
    print(test.iloc[0][TEXT_column][:80], '...')
    print('- unique PMID in train data:', len(train.pmid.unique()), ' for test data:', len(test.pmid.unique()), '\n')
    return train, test


def train_model(train, model_file_to_save, epochs=3, val_frac=0.1):
    X_train = train[TEXT_column]
    y_train = train['label']

    max_seq_length, train_batch_size, lr = 128, 32, 2e-5

    model = BertClassifier(bert_model=BERT_MODEL, random_state=RANDOM_STATE, \
                            max_seq_length=max_seq_length, \
                            train_batch_size=train_batch_size, learning_rate=lr, \
                            epochs=epochs, validation_fraction=val_frac)
    print(model)
    model.fit(X_train, y_train)
    if 0:
        model.save(model_file_to_save)
        print(f'\n- model saved to: {model_file_to_save}\n')
    return model


def train_kfold():
    y_test_all, y_pred_all = [], []
    results = []
    df_out_proba = None
    for fold in range(K):
        train_data, test_data = get_train_test_data(fold)

        model_file = get_model_bin_file(fold)

        val_frac = 0
        model = train_model(train_data, model_file, epochs=EPOCHS, val_frac=val_frac)

        X_test = test_data[TEXT_column]
        y_test = test_data['label']
        y_test_all += y_test.tolist()

        y_pred = model.predict(X_test)
        y_proba = model.predict_proba(X_test)
        del model

        y_pred_all += y_pred.tolist()

        tmp = pd.DataFrame(data=y_proba, columns=[f'c{i}' for i in range(NUM_CLASSES)])
        tmp['confidence'] = tmp.max(axis=1)
        tmp['winner'] = tmp.idxmax(axis=1)
        tmp[TEXT_column] = X_test.tolist()
        tmp['label'] = y_test.tolist()
        df_out_proba = tmp if df_out_proba is None else pd.concat((df_out_proba, tmp))

        acc = accuracy_score(y_pred, y_test)
        res = precision_recall_fscore_support(y_test, y_pred, average='macro')
        print(f'\nAcc: {acc:.3f}      F1:{res[2]:.3f}       P: {res[0]:.3f}   R: {res[1]:.3f} \n')

        #item = {'Acc': acc, 'weight': len(test_data)/len(df), 'size': len(test_data)}
        item = {'Acc': acc, 'weight': 1, 'size': len(test_data)}
        item.update({'P':res[0], 'R':res[1], 'F1':res[2]})
        for cls in np.unique(y_test):
            res = precision_recall_fscore_support(y_test, y_pred, average=None, labels=[cls])
            for i, scoring in enumerate('P R F1'.split()):
                item['{}_{}'.format(scoring, cls)] = res[i][0]
        results.append(item)

        acc_all = np.mean(np.array(y_pred_all) == np.array(y_test_all))
        res = precision_recall_fscore_support(y_test_all, y_pred_all, average='macro')
        print( f'\nAVG of {fold+1} folds  |  Acc: {acc_all:.3f}    F1:{res[2]:.3f}       P: {res[0]:.3f}   R: {res[1]:.3f} \n')

    # show an overview of the performance
    df_2 = pd.DataFrame(list(results)).transpose()
    df_2['avg'] = df_2.mean(axis=1)
    df_2 = df_2.transpose()
    df_2['size'] = df_2['size'].astype(int)
    display(df_2)

    # put together the results of all 5-fold tests and save
    output_pred_csv_file_train = get_pred_csv_file(mode='train')
    df_out_proba.to_csv(output_pred_csv_file_train, index=False, float_format="%.3f")
    print(f'\noutput all {K}-fold test results to: "{output_pred_csv_file_train}"\n')


def evaluate_and_error_analysis():
    df = pd.read_csv(get_pred_csv_file(mode='train')) # -2: a flag indicating putting together the results on all folds
    df['pred'] = df['winner'].apply(lambda x:int(x[1])) # from c0->0, c1->1, c2->2, c3->3

    print(f'\nK={K}  EPOCHS={EPOCHS}  text_column={TEXT_column}  split_by={SPLIT_by}\n')
    #print('\nConfusion Matrix:\n')
    cm = confusion_matrix(df.label, df.pred)
    print(cm)

    print('\nClassification Report:\n')
    print(classification_report(df.label, df.pred, digits=3))


def main():
    task_func = {
        'train_kfold': train_kfold,
        'evaluate_and_error_analysis': evaluate_and_error_analysis,
    }

    task = 'train_kfold'
    task_func[task]()
    task = 'evaluate_and_error_analysis'
    task_func[task]()


if __name__ == "__main__":
    tic = time.time()
    main()
    print(f'time used: {time.time()-tic:.0f} seconds')
